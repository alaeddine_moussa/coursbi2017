package association;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Category
 *
 */
@Entity

public class Category implements Serializable {

	@Id
	private int idCategory;
	private String nom;
	private static final long serialVersionUID = 1L;
	//association
	@OneToMany(mappedBy="parentCategory")
	private List<Category> sousCategories;
	@ManyToOne
	private Category parentCategory;

	public Category() {
		super();
	}   
	public int getIdCategory() {
		return this.idCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	public List<Category> getSousCategories() {
		return sousCategories;
	}
	public void setSousCategories(List<Category> sousCategories) {
		this.sousCategories = sousCategories;
	}
	public Category getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(Category parentCategory) {
		this.parentCategory = parentCategory;
	}
   
}
