package association;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Client
 *
 */
@Entity

public class Client implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idClient;
	private String nom;
	private static final long serialVersionUID = 1L;
	
	//association
	@OneToMany(mappedBy="client",cascade=CascadeType.PERSIST,fetch=FetchType.EAGER)
	private List<Compte> listComptes;

	public Client() {
		super();
	}   
	public int getIdClient() {
		return this.idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	public List<Compte> getListComptes() {
		return listComptes;
	}
	public void setListComptes(List<Compte> listComptes) {
		this.listComptes = listComptes;
	}
	
	public void affectClientToCompte(List<Compte> listComptes){
		for (Compte compte : listComptes) {
			compte.setClient(this);
		}
		this.setListComptes(listComptes);
	}
   
}
