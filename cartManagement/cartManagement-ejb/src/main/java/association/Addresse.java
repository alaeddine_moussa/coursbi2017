package association;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Addresse
 *
 */
@Entity

public class Addresse implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idAddresse;
	private String codePostal;
	private static final long serialVersionUID = 1L;

	
	// association
	@OneToOne(mappedBy = "addresse", cascade = CascadeType.PERSIST)
	private Personne personne;

	public Addresse() {
		super();
	}

	public int getIdAddresse() {
		return this.idAddresse;
	}

	public void setIdAddresse(int idAddresse) {
		this.idAddresse = idAddresse;
	}

	public String getCodePostal() {
		return this.codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

}
