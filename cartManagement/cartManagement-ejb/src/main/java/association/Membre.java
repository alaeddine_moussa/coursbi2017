package association;

import association.Utilisateur;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Membre
 *
 */
@Entity

public class Membre extends Utilisateur implements Serializable {

	
	private String roleMembre;
	private static final long serialVersionUID = 1L;

	public Membre() {
		super();
	}   
	public String getRoleMembre() {
		return this.roleMembre;
	}

	public void setRoleMembre(String roleMembre) {
		this.roleMembre = roleMembre;
	}
   
}
