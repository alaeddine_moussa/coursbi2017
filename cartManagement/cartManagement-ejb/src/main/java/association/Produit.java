package association;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Produit
 *
 */
@Entity

public class Produit implements Serializable {

	@Id
	private int idProduit;
	private String designation;
	private static final long serialVersionUID = 1L;
	//associationan
	@ManyToMany
	private List<Magazin> listMagazins;

	public Produit() {
		super();
	}   
	public int getIdProduit() {
		return this.idProduit;
	}

	public void setIdProduit(int idProduit) {
		this.idProduit = idProduit;
	}   
	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public List<Magazin> getListMagazins() {
		return listMagazins;
	}
	public void setListMagazins(List<Magazin> listMagazins) {
		this.listMagazins = listMagazins;
	}
   
}
