package association;

import association.Utilisateur;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Admin
 *
 */
@Entity

public class Admin extends Utilisateur implements Serializable {

	
	private String photoAdmin;
	private static final long serialVersionUID = 1L;

	public Admin() {
		super();
	}   
	public String getPhotoAdmin() {
		return this.photoAdmin;
	}

	public void setPhotoAdmin(String photoAdmin) {
		this.photoAdmin = photoAdmin;
	}
   
}
