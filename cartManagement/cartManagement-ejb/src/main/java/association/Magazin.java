package association;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Magazin
 *
 */
@Entity

public class Magazin implements Serializable {

	@Id
	private int idMagazin;
	private String nom;
	private static final long serialVersionUID = 1L;
	//association
	@ManyToMany(mappedBy="listMagazins")
	private List<Produit> listProduits;

	public Magazin() {
		super();
	}   
	public int getIdMagazin() {
		return this.idMagazin;
	}

	public void setIdMagazin(int idMagazin) {
		this.idMagazin = idMagazin;
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	public List<Produit> getListProduits() {
		return listProduits;
	}
	public void setListProduits(List<Produit> listProduits) {
		this.listProduits = listProduits;
	}
   
}
