package services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import association.Addresse;
import association.Client;
import association.Personne;
import domain.Item;

/**
 * Session Bean implementation class CartManagement
 */
@Stateless
public class CartManagement implements CartManagementRemote {

	@PersistenceContext
	private EntityManager entityManager;

	private List<Item> lisItems = new ArrayList<Item>();
	private double priceT;

	/**
	 * Default constructor.
	 */
	public CartManagement() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addItem(Item i) {
		lisItems.add(i);
		priceT += i.getPrice();

	}

	@Override
	public double totalPrice() {

		return priceT;
	}

	public double getPriceT() {
		return priceT;
	}

	public void setPriceT(double priceT) {
		this.priceT = priceT;
	}

	@Override
	public void createItem(Item item) {
		entityManager.persist(item);

	}

	@Override
	public void updateItem(Item item) {
		entityManager.merge(item);

	}

	@Override
	public Item findItemById(int idItem) {
		return entityManager.find(Item.class, idItem);
	}

	@Override
	public void deleteItem(Item item) {
		entityManager.remove(entityManager.merge(item));

	}

	@Override
	public void createPerson(Personne p) {
		entityManager.persist(p);

	}

	@Override
	public void createAdress(Addresse addresse) {
		entityManager.persist(addresse);

	}

	@Override
	public void createClient(Client client) {
		entityManager.persist(client);

	}

	@Override
	public List<Personne> findAllPerson() {
		return entityManager.createQuery("select p from Personne p", Personne.class).getResultList();
	}

	@Override
	public List<Client> findAllClients() {

		return entityManager.createQuery("select c from Client c", Client.class).getResultList();
	}

	@Override
	public Personne findPersonByid(int idPerson) {

		return entityManager.find(Personne.class, idPerson);
	}

	@Override
	public Client findClinetById(int idClient) {

		return entityManager.find(Client.class, idClient);
	}

	@Override
	public Personne findPersonByName(String namep) {
		return entityManager.createQuery("select p from Personne p where p.nom=?1", Personne.class)
				.setParameter(1, namep).getSingleResult();
	}

	@Override
	public List<Client> findClientByCompteType(String typeCompte) {

		return entityManager
				.createQuery("select c from Client c join c.listComptes lc where lc.typeCompte=?1", Client.class)
				.setParameter(1, typeCompte)
				.getResultList();
	}

}
