package services;

import java.util.List;

import javax.ejb.Remote;

import association.Addresse;
import association.Client;
import association.Personne;
import domain.Item;

@Remote
public interface CartManagementRemote {
	public void addItem(Item i);

	public double totalPrice();

	// Service Item
	public void createItem(Item item);

	public void updateItem(Item item);

	public Item findItemById(int idItem);

	public void deleteItem(Item item);

	// oneToOne
	public void createPerson(Personne p);

	public Personne findPersonByid(int idPerson);

	public Personne findPersonByName(String namep);

	// oneTomany
	public void createClient(Client client);

	public Client findClinetById(int idClient);

	public void createAdress(Addresse addresse);

	// jpql
	public List<Personne> findAllPerson();

	public List<Client> findAllClients();

	public List<Client> findClientByCompteType(String typeCompte);

}
