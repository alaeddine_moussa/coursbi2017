package clientCart;

import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import association.Client;
import association.Compte;
import services.CartManagementRemote;

public class TestOneToMany {

	public static void main(String[] args) throws NamingException {
		InitialContext ctx = new InitialContext();
		CartManagementRemote proxy = (CartManagementRemote) ctx
				.lookup("/cartManagement-ear/cartManagement-ejb/CartManagement!services.CartManagementRemote");
		// Client client = new Client();
		// client.setNom("aaaa");
		//
		// Compte compte1 = new Compte();
		// compte1.setTypeCompte("courant");
		// //compte1.setClient(client);
		// Compte compte2 = new Compte();
		// compte2.setTypeCompte("epargne");
		// //compte2.setClient(client);
		//
		// List<Compte> listComptes = new ArrayList<Compte>();
		// listComptes.add(compte1);
		// listComptes.add(compte2);
		//
		// client.affectClientToCompte(listComptes);
		//
		// //client.setListComptes(listComptes);
		//
		// proxy.createClient(client);

		// find client by id

		// Client c = proxy.findClinetById(1);
		// List<Compte> listComptes = c.getListComptes();
		// for (Compte compte : listComptes) {
		// System.out.println(compte.getTypeCompte());
		// }

		List<Client> listClients = proxy.findClientByCompteType("epa");
		for (Client client : listClients) {
			System.out.println("id\t" + client.getIdClient() + "\tnom\t" + client.getNom());
		}

	}

}
